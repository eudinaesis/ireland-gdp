% GDP: Complications
% Peter Northup - Hertha Firnberg Schulen
% 14.12.2017

# Nigeria: 89% growth?

## Nigeria revised its GDP estimate by 89% in 2014
- $270b => $510b
- Was this "real"?
- No: statistical artifact

## "Rebasing"
- At "base" year 1, 50% farms, 50% oil
- 10% farm growth, 20% oil growth => 15% total growth
- But what if phones have 300% growth, but you don't survey any phone companies?
- Relative to previous base year (1990): 33 => 46 categories, agriculture 35% to 22% GDP, services 29% => 52%, oil/gas 32% => 15% 

## Why does this matter?
- Doesn't change anything "real" ...
- Taxation to GDP ratio: previously 7.8%, now 14.6%
- Oil sector tax/GDP: 27%; everything else: 4.6%

# GDP: the Irish Case

## 2015: 34.7% nominal GDP growth, 25.6% real
![](ie-gdp-2015.jpeg){ .stretch }

## How is this possible? Is it "real"?
That depends on what we mean by "real"--but probably not.

## Why?
![From NIE 2015 Explanatory Note](2015-gdp-flowchart.jpeg)

## Extra €300b "capital"
![From Tierney, p.19](capital-stock.jpeg){ width=40% }

-------------

![From Tierney, p.21](gdp-income.jpeg)

-------------

![From Tierney, p.22](gdp-expenditure.jpeg)

## Trading profits / net exports?
- "Contract manufacturing" results in higher "exports" registered to Ireland, even if no physical item is ever in Ireland 

-------------

![From Tierney, p.27](contract-mfg.jpeg){ width=80% }

# Does it matter?

## How might it matter?
- Eurozone Fiscal Compact rules
![From Tierney, p.18](debt-gdp.jpeg){ width=50% }

## What would you do?
- If you were the head of Ireland's Central Statistics Office, what would you do?

## Alternative measures
* Net National Income: take out depreciation => 10.3% NNI growth 2015.
* Modified Gross National Income (GNI*)

## Calculating GNI*
	* 	Start with Gross Domestic Product (GDP)
	* 	add net factor income from the rest of the world
	* 	to give Gross National Product (GNP)
	* 	add EU subsidies and subtract EU taxes
	* 	to give Gross National Income (GNI)
	* 	then adjust for the :
	* 	– factor income of redomiciled companies
	* 	– depreciation on R&D related intellectual property imports, and
	* 	– depreciation on aircraft for leasing
	* 	to give Modified Gross National Income (GNI*)
(from Coffey)

## Comparison, 2016
- GDP: €276b
- GNI*: €189b
- NNI: €165

Big difference!

## What is it good for?
* GNI*: "probably a good measure of 'national income' and will be a useful denominator for ratios where income matters such as debt ratios" -- but misses some of the tax base
* 11.9% (2015) & 9.4% (2016) GNI* growth rates -- still implausibly large


::: notes

GNI* is probably a good measure of the level of ‘national income’ and will be a useful denominator for ratios where income matters such as debt ratios etc.  However, GNI* does not fully represent the ‘tax base’ as we do get to levy 12.5 per cent Corporation Tax on the outbound profits of MNCs (albeit after a chunky deduction for depreciation).  Thus a ratio of tax to GNI* would overstate the tax burden as part of the tax base is excluded from the denominator while the tax collected from the excluded amount would be included in the numerator.

:::

## Sources
* Daniel Tierney (MIT Sloan School), "Finding Gold in 'Leprechaun Economics'" at <https://daniel-tierney.net/wp-content/uploads/2017/05/15.961-Finding-Gold-in-Leprechaun-Economics-Daniel-Tierney-FINAL.pdf> 
* Seamus Coffey (University College Cork & Irish Fiscal Advisory Council), "That 26% growth rate – from startled earwigs to stars in our eyes" at <http://www.irisheconomy.ie/index.php/2017/08/24/that-26-growth-rate-from-startled-earwigs-to-stars-in-our-eyes/>
