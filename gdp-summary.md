% GDP Complications: Summary
% Peter Northup - Hertha Firnberg Schulen
% 11.1.2018

# Complications of GDP measurement

## GDP surveys and rebasing

We can't simply measure GDP with a thermometer. Calculating the value of all goods/services produced in a year is complicated. It's easier if you have a strong tax system, and a small informal sector.

It's often necessary to rely on representative surveys ... but how do you know your survey is representative? You need to "rebase" your calculation every few years.

### "Rebasing" example
- Suppose in your "base" year, you have 49% farms, 49% oil, 2% everything else
- As a result, you only survey farms & the oil sector
- Suppose 10% farm growth, 20% oil growth. You'll assume ~15% total growth
- But what if, since the base year, "everything else" has grown enormously, but you don't know because you weren't surveying any other businesses?
- That happened in Nigeria, when it rebased from 1990 to 2010.
- Relative to previous base year (1990): 33 => 46 categories; agriculture 35% to 22% GDP; services 29% => 52%; oil/gas 32% => 15%

### Implications

Even though the rebasing didn't change anyone's actual wealth, now outsiders have a better sense of the overall picture -- we see that Nigeria isn't as oil-dependent as we thought.

- That may have implications for tax policy.

## GDP: the Irish Case

### 2015: 34.7% nominal GDP growth, 25.6% real

Was "Ireland" producing 25% more stuff in 2015 as in 2014? Not really. So what happened?

Changes in tax policy encouraged a few companies (Apple and pharmaceutical companies) to "move" intangible assets (patents, copyrights) to subsidiaries in Ireland.

As a result, the Irish subsidiary receives the profits that come from licensing the assets abroad. This counts as "exports", even if nothing is made in Ireland. But some of this profit is reported as "depreciation" on the assets.

### So is this "real" growth?

It depends what you're using GDP growth for.

For the Irish tax authorities, these extra profits are "real" -- it means an extra €2.2b in corporate tax in 2015!

On the other hand, those same assets might move somewhere else next year. They're very mobile.

And if you care about GDP as a measure of what *people in Ireland* are producing, this 25% increase is misleading.

### If you were the head of Ireland's Central Statistics Office, what would you do?

Ireland has to publish standard GDP statistics due to various treaties (EU, etc.). But you can also use alternative measures.

### Some alternatives
- Net National Income: take out depreciation entirely => 10.3% NNI growth 2015.
- Modified Gross National Income (GNI*) => "top-down" adjustment to take account of intangibles' depreciation; still 12% growth in 2015, so still "too big"
- Various indicators of capacities or flourishing: e.g. United Nations Human Development Index

## Conclusions

### "The map is not the territory"

All simplifications and abstractions leave out information. But macroeconomic statistics can be particularly misleading, because "the economy" isn't a clearly distinct thing the same way that an elephant is.

But national economies don't have a "height" or "weight" that you could measure, because they are simply aggregations of all the activities of hundreds of thousands, millions, or even billions of people. This gives rise to three sorts of problems.

The first is how much to value each activity, especially those that don't have a price. Issues here include unpaid housework, Gross National Happiness, etc. The second is that measuring all this activity is actually an enormous technical challenge (hence the Nigeria rebasing).

The third class of problems, which the Irish case illustrates, comes from the fragility of our categorizations. The boundary between two elephants is pretty clear, but an invisible "intangible asset" can be "moved" from one country to another with the press of a button.

So always ask: *why* are you using a particular statistic? Is it the right one for the question at hand?

## Sources
* Seamus Coffey (University College Cork & Irish Fiscal Advisory Council), "That 26% growth rate – from startled earwigs to stars in our eyes" at <http://www.irisheconomy.ie/index.php/2017/08/24/that-26-growth-rate-from-startled-earwigs-to-stars-in-our-eyes/>
* Daniel Tierney (MIT Sloan School), "Finding Gold in 'Leprechaun Economics'" at <https://daniel-tierney.net/wp-content/uploads/2017/05/15.961-Finding-Gold-in-Leprechaun-Economics-Daniel-Tierney-FINAL.pdf> 
* Amadou Sy (Brookings Institute), "Are African countries rebasing GDP in 2014 finding evidence of structural transformation?" at <https://www.brookings.edu/blog/africa-in-focus/2015/03/03/are-african-countries-rebasing-gdp-in-2014-finding-evidence-of-structural-transformation/>